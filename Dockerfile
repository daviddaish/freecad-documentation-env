FROM registry.gitlab.com/daviddaish/freecad_docker_env:latest

RUN apt install -y doxygen entr graphviz sass

RUN python -m pip install sphinx breathe gitpython lbt-ladybug
